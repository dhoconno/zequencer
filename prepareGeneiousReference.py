import os
from Bio import SeqIO
import subprocess
from shutil import copyfile
import argparse

# requires BioPython
# install with ~/anaconda/bin/pip install biopython

# accept command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("gff", help="GFF3 annotation file exported from Geneious")
parser.add_argument("fasta", help="FASTA sequence file exported from Geneious")
parser.add_argument("ref_name", help="Specify name of reference genome")
args = parser.parse_args()

# arguments
appRoot = os.path.dirname(os.path.realpath(__file__))
gff = args.gff
fasta = args.fasta
seq_id = args.ref_name

def copyReferenceFiles():
    '''
    Copy GFF and FASTA files to reference sequence folder
    '''

    # create reference folder for seq_id if it does not already exist
    if not os.path.exists(appRoot + '/ref/' + seq_id):
        os.makedirs(appRoot + '/ref/' + seq_id)

    # copy GFF and FASTA files
    copyfile(gff, appRoot + '/ref/' + seq_id + '/' + seq_id + '.gff')
    copyfile(gff, appRoot + '/ref/' + seq_id + '/' + 'genes.gff')
    copyfile(fasta, appRoot + '/ref/' + seq_id + '/' + seq_id + '.fa')
    copyfile(fasta, appRoot + '/ref/' + seq_id + '/' + 'sequences.fa')

    gff_filename = appRoot + '/ref/' + seq_id + '/' + seq_id + '.gff'
    fasta_filename = appRoot + '/ref/' + seq_id + '/' + seq_id + '.fa'

    return (gff_filename, fasta_filename)

def indexFasta(fasta_filename):
    '''
    if FASTA index doesn't exist, run samtools faidx
    '''

    faidx_filename = fasta_filename + '.fai'
    if not os.path.isfile(faidx_filename):
        subprocess.call([appRoot + '/bin/samtools',
                        'faidx',
                        fasta_filename])

def createNovoindex(fasta_filename):
    '''
    if Novoalign index doesn't exist, create it
    '''

    novoindex_filename = fasta_filename + '.nix'
    if not os.path.isfile(novoindex_filename):
        subprocess.call([appRoot + '/bin/novoindex',
                        novoindex_filename,
                        fasta_filename])

    return novoindex_filename

def createSnpeffDb():
    '''
    add genome to SnpEff config file and create required SnpEff database
    '''

    # check whether sequence id is already in snpEff config file
    if seq_id in open(appRoot + '/bin/snpEff.config').read():
        add_sequence = False
    else:
        add_sequence = True

    # append genome definition to snpEff config if sequence is not in file
    if add_sequence is True:
        with open(appRoot + '/bin/snpEff.config', "a") as myfile:
            myfile.write('\n' + seq_id + '.genome : ' + 'Added from Geneious')

    # create snpeff database
    cmd = ['java',
                   '-jar',
                   appRoot + '/bin/snpEff.jar',
                   'build',
                   '-gff3',
                   seq_id,
                   '-v']

    subprocess.call(cmd)

    # remove temporary snpeff files
    os.remove(appRoot + '/ref/' + seq_id + '/' + 'genes.gff')
    os.remove(appRoot + '/ref/' + seq_id + '/' + 'sequences.fa')

def makeRefIndices():
    ncbi_fetch = copyReferenceFiles()
    indexFasta(ncbi_fetch[1])
    createNovoindex(ncbi_fetch[1])
    createSnpeffDb()

# prepare indices
makeRefIndices()